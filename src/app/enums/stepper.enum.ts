export enum StepState {
  Current = 'current',
  Past = 'past',
  Future = 'future'
}

export enum NextButtonLabel {
  Next = 'Next step',
  Finish = 'Finish'
}

export enum Directions {
  Horizontal = 'horizontal',
  Vertical = 'vertical'
}
