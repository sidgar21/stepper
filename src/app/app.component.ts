import { Component } from '@angular/core';
import {Directions, NextButtonLabel} from "./enums/stepper.enum";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public steps = [
    {
      label: 'Role'
    },
    {
      label: 'Email'
    },
    {
      label: 'Settings'
    }];

  public currentStepIndex = 0;

  public buttonLabel = NextButtonLabel.Next;

  public directions = Directions;

  public nextStep() {
    if (this.currentStepIndex < (this.steps.length - 1)) {
      this.currentStepIndex++;
      if (this.currentStepIndex === (this.steps.length - 1)) {
        this.buttonLabel = NextButtonLabel.Finish;
      }
    } else {
      this.currentStepIndex = 0;
      this.buttonLabel = NextButtonLabel.Next;
    }
  }
}
