import {Component, Input} from '@angular/core';
import {IStep} from "../interfaces/step.interface";
import {Directions} from "../enums/stepper.enum";

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent {
  @Input() steps: IStep[] = [];
  @Input() currentStep!: number;
  @Input() direction = Directions.Horizontal;
  @Input() color = '#22ECE9';

  public directions = Directions;
}
