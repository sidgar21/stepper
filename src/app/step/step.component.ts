import {Component, Input, OnChanges} from '@angular/core';
import {IStep} from "../interfaces/step.interface";
import {Directions, StepState} from "../enums/stepper.enum";

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss']
})
export class StepComponent implements OnChanges {
  @Input() step: IStep | undefined;
  @Input() isCurrentStep = false;
  @Input() isPastStep = false;
  @Input() stepIndex = 0;
  @Input() color = '';
  @Input() direction = Directions.Horizontal;

  public directions = Directions;

  public stepMarker: StepState = StepState.Future;

  ngOnChanges() {
    this.stepMarker = this.isCurrentStep ? StepState.Current : this.isPastStep ? StepState.Past : StepState.Future;
  }
}
